<?php

namespace app\controllers;

use Yii;
use yii\filters\AccessControl;
use yii\web\Controller;
use yii\web\Response;
use yii\filters\VerbFilter;
use app\models\Request;
use app\models\UserWallet;

class PaymentEmulatorController extends Controller
{
    /*
        Эмулятор платежной системы.
        Рандомно генерирует от 1 до 10 запросов в формате:
        {
            id: идентификатор транзакции 
            sum: сумма (от 10р. до 500р.)
            commision: коммиссия (от 0,5% до 2%)
            order_number: идентификатор клиента (от 1 до 20)
        }
        Сохраняет данные локально.
        Делает цифровую подпись (механизм на усмотрение соискателя)
        Отправляет пакетом с интервалом 20 секунд на второй сервис
        Повторяет циклично
    */
    public function actionSend(){
        $requestRandomNumber = rand(1,10);
        $requests = [];

        for($i = 1; $i <= $requestRandomNumber; ++$i) {
            $requests[] = [
                'id' => Yii::$app->Helper->generateTransactionID(),
                'sum' => Yii::$app->Helper->generateSum(),
                'commision' => Yii::$app->Helper->generateCommision(),
                'order_number' => Yii::$app->Helper->generateOrderNumber()
            ];
        }

        $token = Yii::$app->Helper->generateToken();
        $serialized = serialize($requests);
        $model = new Request();
        $model->signature = $token;
        $model->request_text = $serialized;
        $model->save();
        echo "Request has been sent!";
    }

    /*
        Эмулятор приема платежа
        Получает запрос
        Проверяет подпись
        Парсит пакет данных и добавляет данные в очередь обработки
        Высчитывает сумму с коммиссией
        Сохраняет в локальную базу в формате 
        {
            id: идентификатор транзакции
        user_id: идентификатор клиента
        sum: сумма с учетом коммиссии
        }
        и добавляет запись в таблицу user_wallet либо наращивает сумму в записи в формате 
        {
        user_id: идентификатор пользователя
        sum: сумма на счету
    }*/
    public function actionGet() {
        $request = Request::find()->orderBy('id DESC')->limit(1,1)->one();
        $signature = $request['signature'];
        if(strlen($signature) === 32){
            $requestTextData = unserialize($request['request_text']);
            foreach($requestTextData as $textData) {
                $userWalletModel = new UserWallet();
                $userWalletModel->transaction_id = $textData['id'];
                $userWalletModel->user_id = $textData['order_number'];
                $userWalletModel->sum = $textData['sum'] * $textData['commision'] / 100;
                $userWalletModel->save();
            }
        }
        echo "Response has been received!";
    }
}
