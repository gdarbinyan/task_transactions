<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_user_wallet".
 *
 * @property int $id
 * @property int $transaction_id
 * @property int $user_id
 * @property double $sum
 */
class UserWallet extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_user_wallet';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['transaction_id', 'user_id'], 'integer'],
            [['sum'], 'number'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'transaction_id' => 'Transaction ID',
            'user_id' => 'User ID',
            'sum' => 'Sum',
        ];
    }
}
