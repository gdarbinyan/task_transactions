<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tbl_request".
 *
 * @property int $id
 * @property string $signature
 * @property string $request_text
 */
class Request extends \yii\db\ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return 'tbl_request';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['signature', 'request_text'], 'string'],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'signature' => 'Signature',
            'request_text' => 'Request Text',
        ];
    }
}
