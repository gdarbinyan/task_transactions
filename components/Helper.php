<?php
namespace app\components;

use yii\base\Component;

class Helper extends Component {

    /**
     * generates transaction ID
     */
    public function generateTransactionID() 
    {
        mt_srand((double)microtime()*10000);
        $charid = md5(uniqid(rand(), true));
        $c = unpack("C*",$charid);
        $c = implode("",$c);
        return substr($c,0,20);
    }

    /**
     * generates sum
     */
    public function generateSum() : int 
    {
        return rand(10, 500);
    }

    /**
     * generates commision
     */
    public function generateCommision() : float 
    {
        return rand(0.5, 2);
    }

    /**
     * generates order number
     */
    public function generateOrderNumber() : int 
    {
        return rand(1, 20);
    }

    /**
     * generates token
     */
    public function generateToken() : string
    {
        return bin2hex(random_bytes(16));
    }

}